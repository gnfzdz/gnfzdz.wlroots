# Ansible Role - gnfzdz.wlroots.screenlock

Install lockscreen packages and configure lock on idle and keybindings

## Variables
-------

Variable | Description | Type | Default
-------- | ----------- | -------- | --------
`wlroots_screenlock_packages` | A list of packages to install for screenlocking | list of str | [ swayidle', 'swaylock' ]
`wlroots_screenlock_softlock_time` | Idle time in seconds before executing `wlroots_screenlock_light_exec` command | int | 1800
`wlroots_screenlock_hardlock_time` | Idle time in seconds before executing `wlroots_screenlock_dark_exec` command and turning off displays | int | 3600
`wlroots_screenlock_light_exec` | A command to execute to lock the screen with a partially transparent overlay | str | swaylock -f -c 00000080
`wlroots_screenlock_light_binding` | A binding to immediately lock the screen with a partially transparent overlay | str | \$mod+z
`wlroots_screenlock_dark_exec` | A command to execute to lock the screen with a non-transparent overlay | str | swaylock -f -c 000000ff
`wlroots_screenlock_dark_binding` | A binding to immediately lock the screen with a non-transparent overlay | str | \$mod+Shift+z
