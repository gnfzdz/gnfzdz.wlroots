# Ansible Role - gnfzdz.workstation.sway

Install and configure the Sway wayland compositor and supporting tools

## Variables
-------

Variable | Description | Type | Default
-------- | ----------- | -------- | --------
`wlroots_sway_packages` | A list of core packages to install for sway | list of str | [ 'sway', 'swaybg' ]
