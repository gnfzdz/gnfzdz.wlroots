# Ansible Role - gnfzdz.workstation.sway

Install and configure common dependencies for the all roles in this collection

## Variables
-------

Variable | Description | Type | Default
-------- | ----------- | -------- | --------
`wlroots_common_packages` | A list of core packages to install for wlroots | list of str | `xdg-desktop-portal-wlroots` and a terminal emulator (`alacritty` if available else `foot`)
`wlroots_common_mod` | Indicates the modifier key used as a prefix for most keybindings | Mod4
`wlroots_common_terminal` | Specifies (but does not install) the default terminal to be used by other roles | str | `alacritty` if available else `foot`
`wlroots_common_configd_sway` | Specifies a directory where helpful Sway configuration includes will be installed for potential inclusion by the main Sway configuration file. | str | /usr/local/share/sway/config.d
