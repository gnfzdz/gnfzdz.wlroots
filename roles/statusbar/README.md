# Ansible Role - gnfzdz.wlroots.statusbar

Install and configure waybar for use with wlroots compositors

## Variables
-------

Variable | Description | Type | Default
-------- | ----------- | -------- | --------
`wlroots_statusbar_packages` | A list of packages to install providing the status bar and supporting utilities | list of str | [ 'waybar' ]
`wlroots_statusbar_command` | A command to start the status bar | str | waybar
