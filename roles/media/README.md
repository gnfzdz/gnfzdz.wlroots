# Ansible Role - gnfzdz.wlroots.media

Install utilities to manage media playback and configure bindings.

## Variables
-------

Variable | Description | Type | Default
-------- | ----------- | -------- | --------
`wlroots_media_packages` | A list of packages to install for audio management | list of str | [ 'playerctl' ]
`wlroots_media_playpause_binding` | A keybinding used to toggle playback status between play and pause | str | XF86AudioPlay
`wlroots_media_playpause_exec` | A command executed to toggle playback status between play and pause | str | playerctl play-pause
`wlroots_media_next_binding` | A keybinding used to seek the next media item | str | XF86AudioNext
`wlroots_media_next_exec` | A command executed to seek the next media item | str | playerctl next
`wlroots_media_previous_binding` | A keybinding used to seek the previous media item | str | XF86AudioPrev
`wlroots_media_previous_exec` | A command executed to seek the previous media item | str | playerctl previous
