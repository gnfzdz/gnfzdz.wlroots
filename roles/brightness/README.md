# Ansible Role - gnfzdz.wlroots.brightness

Install utilities to manage screen brightness and configure bindings.

## Variables
-------

Variable | Description | Type | Default
-------- | ----------- | -------- | --------
`wlroots_brightness_packages` | A list of packages to install for audio management | list of str | [ 'brightnessctl' ]
`wlroots_brightness_raise_binding` | A keybinding used to raise display brightness | str | XF86MonBrightnessUp
`wlroots_brightness_raise_exec` | A command executed to raise display brightness | str | brightnessctl set 5%+
`wlroots_brightness_lower_binding` | A keybinding used to lower display brightness | str | XF86MonBrightnessDown
`wlroots_brightness_lower_exec` | A command executed to lower display brightness | str | brightnessctl set 5%-
