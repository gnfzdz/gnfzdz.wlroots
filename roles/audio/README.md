# Ansible Role - gnfzdz.wlroots.audio

Install utilities for audio playback/recording and configure bindings.

## Variables
-------

Variable | Description | Type | Default
-------- | ----------- | -------- | --------
`wlroots_audio_packages` | A list of packages to install for audio management | list of str | Distribution specific package providing `pactl`
`wlroots_audio_raise_binding` | A keybinding used to raise volume | str | XF86AudioRaiseVolume
`wlroots_audio_raise_exec` | A command executed to raise volume | str | pactl set-sink-volume @DEFAULT_SINK@ +5%
`wlroots_audio_lower_binding` | A keybinding used to lower volume | str | XF86AudioLowerVolume
`wlroots_audio_lower_exec` | A command executed to lower volume | str | pactl set-sink-volume @DEFAULT_SINK@ -5%
`wlroots_audio_mute_binding` | A keybinding used to mute volume | str | XF86AudioMute
`wlroots_audio_mute_exec` | A command executed to mute volume | str | pactl set-sink-mute @DEFAULT_SINK@ toggle
`wlroots_audio_micmute_binding` | A keybinding used to mute the microphone | str | XF86AudioMicMute
`wlroots_audio_micmute_exec` | A command executed to mute the microphone | str | pactl set-source-mute @DEFAULT_SOURCE@ toggle
