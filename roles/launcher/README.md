# Ansible Role - gnfzdz.wlroots.launcher

Install an application launcher and configure keybindings

## Variables
-------

Variable | Description | Type | Default
-------- | ----------- | -------- | --------
`wlroots_launcher_binding` | A keybinding used request opening the launcher | str | \$mod+d
`wlroots_launcher_exec` | A command executed to open the application launcher | str | wofi --show drun
