# Ansible Role - gnfzdz.wlroots.screenshot

Install utilities to manage screenshotting and configure bindings.

## Variables
-------

Variable | Description | Type | Default
-------- | ----------- | -------- | --------
`wlroots_screenshot_packages` | A list of packages to install for screenshotting | list of str | [ 'grim', 'slurp' ]
`wlroots_screenshot_directory` | Path to the directory where captured screenshots will be stored | str | ~/Pictures
`wlroots_screenshot_format` | Provides format for the filename of captured screenshots | str | screenshot_\$(date +'%Y-%m-%d_%H-%M-%S').png
`wlroots_screenshot_full_binding` | A keybinding used to capture the full screen as an image | str | Print
`wlroots_screenshot_full_exec` | A command executed to capture the full screen as an image | str | grim {{ wlroots_screenshot_directory }}/{{ wlroots_screenshot_format }}
`wlroots_screenshot_region_binding` | A keybinding used to capture a region of the screen as an image | str | Shift+Print
`wlroots_screenshot_region_exec` | A command executed to capture a region of the screen as an image | str | grim -g "$(slurp)" {{ wlroots_screenshot_directory }}/{{ wlroots_screenshot_format }}
