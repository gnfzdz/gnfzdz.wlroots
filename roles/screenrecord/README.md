# Ansible Role - gnfzdz.wlroots.screenrecord

Install utilities to manage screen recording and configure bindings.

## Variables
-------

Variable | Description | Type | Default
-------- | ----------- | -------- | --------
`wlroots_screenrecord_packages` | A list of packages to install for screen recording | list of str | [ 'wf-recorder', 'slurp' ]
`wlroots_screenrecord_directory` | Path to the directory where captured recordings will be stored | str | ~/Videos
`wlroots_screenrecord_format` | Provides format for the filename of captured recordings | str | recording_\$(date +'%Y-%m-%d_%H-%M-%S').mp4
`wlroots_screenrecord_full_binding` | A keybinding used to capture the full screen as a video | str | \$mod+Print
`wlroots_screenrecord_full_exec` | A command executed to capture the full screen as a video | str | wf-recorder -f {{ wlroots_screenrecord_directory }}/{{ wlroots_screenrecord_format }}
`wlroots_screenrecord_region_binding` | A keybinding used to capture a region of the screen as a video | str | \$mod+Shift+Print
`wlroots_screenrecord_region_exec` | A command executed to capture a region of the screen as a video | str | wf-recorder -g "$(slurp)" -f {{ wlroots_screenrecord_directory }}/{{ wlroots_screenrecord_format }}
`wlroots_screenrecord_stop_binding` | A keybinding used to stop active screen recording | str | \$mod+Shift+Backspace
`wlroots_screenrecord_stop_exec` | A command executed to stop active screen recording | str | killall -s SIGINT wf-recorder
