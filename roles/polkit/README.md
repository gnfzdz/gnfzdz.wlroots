# Ansible Role - gnfzdz.wlroots.polkit

Install and start a polkit agent for graphical prompts

## Variables
-------

Variable | Description | Type | Default
-------- | ----------- | -------- | --------
`wlroots_polkit_packages` | A list of packages to install providing the polkit agent | list of str | Distribution specific package providing the gnome polkit agent
`wlroots_polkit_command` | A command to start the polkit agent | str | Distribution specific path to the gnome polkit agent
