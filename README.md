# Ansible Collection - gnfzdz.wlroots

A collection of roles for setting up a fully-featured, wlroots based desktop environment. Currently only supports the [Sway](https://swaywm.org/) compositor.

## Roles

Name | Description
-------- | -----------
[gnfzdz.wlroots.autoconfigure](https://gitlab.com/gnfzdz/gnfzdz.wlroots/-/blob/main/roles/all/README.md) | Applies an opinionated set of configurations based on the roles within this collection
[gnfzdz.wlroots.audio](https://gitlab.com/gnfzdz/gnfzdz.wlroots/-/blob/main/roles/audio/README.md) | Install utilities to manage audio playback volume and configure bindings
[gnfzdz.wlroots.brightness](https://gitlab.com/gnfzdz/gnfzdz.wlroots/-/blob/main/roles/brightness/README.md) | Install utilities to manage screen brightness and configure bindings.
[gnfzdz.wlroots.common](https://gitlab.com/gnfzdz/gnfzdz.wlroots/-/blob/main/roles/common/README.md) | Common configuration shared across the gnfzdz.wlroots collection
[gnfzdz.wlroots.launcher](https://gitlab.com/gnfzdz/gnfzdz.wlroots/-/blob/main/roles/launcher/readme.md) | Install an application launcher and configure bindings.
[gnfzdz.wlroots.media](https://gitlab.com/gnfzdz/gnfzdz.wlroots/-/blob/main/roles/media/readme.md) | Install utilities to manage media playback and configure bindings.
[gnfzdz.wlroots.polkit](https://gitlab.com/gnfzdz/gnfzdz.wlroots/-/blob/main/roles/polkit/README.md) | Install and start a polkit agent for graphical prompts
[gnfzdz.wlroots.screenlock](https://gitlab.com/gnfzdz/gnfzdz.wlroots/-/blob/main/roles/screenlock/README.md) | Install lockscreen packages and configure lock on idle and keybindings
[gnfzdz.wlroots.screenrecord](https://gitlab.com/gnfzdz/gnfzdz.wlroots/-/blob/main/roles/screenrecord/README.md) | Install utilities to manage screen recording and configure bindings.
[gnfzdz.wlroots.screenshot](https://gitlab.com/gnfzdz/gnfzdz.wlroots/-/blob/main/roles/screenshot/README.md) | Install utilities to manage screenshotting and configure bindings.
[gnfzdz.wlroots.statusbar](https://gitlab.com/gnfzdz/gnfzdz.wlroots/-/blob/main/roles/statusbar/README.md) | Install and configure waybar for use with wlroots compositors
[gnfzdz.wlroots.sway](https://gitlab.com/gnfzdz/gnfzdz.wlroots/-/blob/main/roles/sway/README.md) | Install and configure the Sway wayland compositor and supporting tools
